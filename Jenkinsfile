def gv

pipeline{
    agent any


    tools{
        maven "Maven"
    }

    environment{
        DOCKER_REPO_SERVER='931977673297.dkr.ecr.eu-west-3.amazonaws.com'
        DOCKER_REPO="${DOCKER_REPO_SERVER}/java-maven-app"
    }
    stages {

        stage('Checkout') {
            steps {
                scmSkip(deleteBuild: false, skipPattern:'.*ci: version bump.*')
            }
        }

        stage("init"){
            steps{
                script{
                    gv = load "script.groovy"
                }
            }
        }
        stage("incrementing app version"){
            steps{
                script{
                    echo 'incrementing app version...'
                    sh 'mvn build-helper:parse-version versions:set -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} versions:commit'
                    def matcher = readFile('pom.xml')=~'<version>(.+)</version>'
                    def version = matcher[1][1]
                    env.IMAGE_NAME = "$version-$BUILD_NUMBER"
                }
            }
        }

        stage("build jar"){
            steps{
                script{
                    gv.buildJar()
                }
            }
        }
        stage("build docker image"){

            steps{
                script{
                    echo 'building docker image and pushing it to ECR ...'
                    withCredentials([usernamePassword(credentialsId:'ecr-credentials', passwordVariable:'PASS', usernameVariable:'USER')]){
                        sh "docker build -t ${DOCKER_REPO}:smja-${IMAGE_NAME} ."
                        sh "echo $PASS | docker login -u $USER --password-stdin ${DOCKER_REPO_SERVER}"
                        sh "docker push  ${DOCKER_REPO}:smja-${IMAGE_NAME}"
                    }
                }
            }
        }
        stage("deploy"){

            environment {
                AWS_ACCESS_KEY_ID = credentials('jenkins-aws-access-key-id')
                AWS_SECRET_ACCESS_KEY = credentials('jenkins-aws-secret-access-key')
                APP_NAME= 'java-maven-app'
            }
            steps {
                script {
                    echo 'deploying docker image...'
                    sh 'envsubst < kubernetes/deployment.yaml | kubectl apply -f -'
                    sh 'envsubst < kubernetes/service.yaml | kubectl apply -f -'
                }
            }             
        }

        stage("commit version update"){
            steps{
                script{
                    withCredentials([usernamePassword(credentialsId:'gitlab-credentials', passwordVariable:'PASS', usernameVariable:'USER')]){
                        sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/hanabenasker/spring-boot-maven-app.git"
                        sh "git add ."
                        sh 'git commit -m "ci: version bump"'
                        sh 'git push origin HEAD:master'
                    }
                }
            }
        }

    }

}
