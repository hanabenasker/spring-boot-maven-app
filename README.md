## Deploying to kubernetes cluster on EKS from a complete CI/CD pipeline with automatic triggering, dynamic versioning for new docker application
This Jenkinsfile is used to build, test, and deploy the Java-Maven-App application using Jenkins. The pipeline is defined using the Jenkins declarative pipeline syntax.

### Pipeline Overview
The pipeline consists of several stages, including:

1. Checkout - This stage skips builds triggered by commits containing "ci: version bump" in the commit message.
2. init - This stage loads a custom Groovy script.
3. incrementing app version - This stage increments the application version and sets the environment variable IMAGE_NAME.
4. build jar - This stage builds the Java jar file using the gv.buildJar() method.
5. build docker image - This stage builds a Docker image and pushes it to the Amazon Elastic Container Registry (ECR).
6. deploy - This stage deploys the Docker image to Kubernetes.
7. commit version update - This stage commits the updated version number to GitLab.
