def buildJar(){
    echo 'building the application ...'
    sh 'mvn clean package'
}

def buildImage(){
    echo 'building docker image and pushing it to dockerhub ...'
    withCredentials([usernamePassword(credentialsId:'dockerhub-credentials', passwordVariable:'PASS', usernameVariable:'USER')]){
        sh' docker build -t hanabenasker/myrepo:smja-1.0 .'
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh 'docker push  hanabenasker/myrepo:smja-1.0'
    }
}

def deployApp(){
    echo "deploying the application ..."

}
return this