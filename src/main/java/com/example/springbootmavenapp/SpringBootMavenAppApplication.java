package com.example.springbootmavenapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootMavenAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootMavenAppApplication.class, args);
    }

}
